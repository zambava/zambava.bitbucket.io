var id_ahli = "1uPYvmDsNy9m6R3bfHGYHv37KMA2mBv3juU2j7v6EJOk",
	id_guru = "1aqGr8_LLX92_g2hcwpkz6hXH9yuyX0MR9N_7nhtrPW0",
	data_header = ["No.", "Features", "Adobe Dreamweaver", "HJCode"],
	data_survey = [],
	HJCode = 0,
	DW = 0;


function getDataGoogle(id){
	var url = "https://spreadsheets.google.com/feeds/cells/" + id + "/od6/public/basic?alt=json-in-script&callback=?";

	$.getJSON(url, function(resp) {
		data_survey = getData(resp);

		var skor_ideal = 5 * data_survey.length * 16;

		for(k in data_survey)
		{
			delete data_survey[k]["Timestamp"];
			
			for(var i in data_survey[k])
			{
				if(typeof data_survey[k][i] == "object")
				{
					HJCode += parseInt(data_survey[k][i][1]);
					DW += parseInt(data_survey[k][i][0]);
				}
			}
		}

		var h = (HJCode/skor_ideal) * 100,
			d = (DW/skor_ideal) * 100;

			console.log(HJCode, DW);

		// console.log((HJCode/skor_ideal) * 100);
		// console.log((DW/skor_ideal) * 100);

		HJCode = 0;
		DW = 0;
	});
}

function getData(resp) {
	//Functions to get row and column from cell reference
	var get_row = function(str) { return /[0-9]{1,10}/.exec(str)*1 },
		get_col = function(str) { return /[A-Z]{1,2}/.exec(str) },
		data = [],
		cols = ["A"],
		cells = resp.feed.entry,
		column_headers = {},
		number_of_cols = 0,
		number_of_rows = get_row(cells[cells.length-1].title.$t),
		row = 1,
		_row = {};
		 
	//Get column headers
	for (var i=0; i < cells.length; i++)
	{
		var _cell_ref = cells[i].title.$t,
			_column = get_col(_cell_ref);

		if (_cell_ref == "A2") {
			break;
		}
		else
		{
			column_headers[_column] = cells[i].content.$t;
			number_of_cols = 0;
		}
	};

	//Iterate cells and build data object
	for (var i=0; i < cells.length; i++)
	{
		var cell_ref = cells[i].title.$t,
			col = get_col(cell_ref),
			value = cells[i].content.$t,
			arr = [];

		if (row < get_row(cell_ref))
		{
			//new row
			data.push(_row);
			_row = {};
			row++;
		}

		arr = !isNaN(value) ? (value > 6 ) ? [1/(value-6), value-6] : (value < 6 ) ? [6-value, 1/(6-value)] : [5, 5] : value;
		_row[column_headers[col]] = arr;
	}

	data.push(_row);
	data.shift();
	return data;
}

$("#id_angket").on("change", function(){
	var val = eval($(this).val());
	
	if (isNaN(val)) getDataGoogle(val);
});